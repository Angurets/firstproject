package com.company;
import java.util.ArrayList;
import java.util.List;


public class HomeTask1 {

    public static void main(String[] args) {
        List<String> states = new ArrayList<>();
        states.add("Ukraine");
        states.add("Italy");
        states.add("Spain");
        states.add("Germany");
        states.add("Canada");
        states.add("Greece");
        states.add("France");
        states.add("Norway");
        states.add("Denmark");
        states.add("Belarus");

        for (String state : states){
            System.out.println(state);
        }

        System.out.println("-----\\-----");

        String newSeven = states.get(2); //declared new variable newSeven, which stores previous value of the third element
        states.set(2,states.get(7)); //using method set we replace value of the second element to 7 element value
        states.set(7, newSeven); //here we're assign value for the 7 element which is equal to the value of previous second element

        for (String state : states){ //cycle forEach
            System.out.println(state);
        }



    }
}
